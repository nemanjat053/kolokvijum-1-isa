package app.kolokvijum.repository;

import org.springframework.data.repository.CrudRepository;

import app.kolokvijum.model.Cas;

public interface CasRepository extends CrudRepository<Cas, Long>{

}
