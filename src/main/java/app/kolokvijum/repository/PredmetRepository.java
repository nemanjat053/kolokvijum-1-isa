package app.kolokvijum.repository;

import org.springframework.data.repository.CrudRepository;

import app.kolokvijum.model.Predmet;

public interface PredmetRepository extends CrudRepository<Predmet, Long>{

}
