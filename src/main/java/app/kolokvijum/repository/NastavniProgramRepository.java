package app.kolokvijum.repository;

import org.springframework.data.repository.CrudRepository;

import app.kolokvijum.model.NastavniProgram;

public interface NastavniProgramRepository extends CrudRepository<NastavniProgram, Long>{

}
